const fastify = require("fastify");

const build = (opts = {}) => {
  const app = fastify(opts);
  app.get("/", (request, reply) => {
    reply.code(200).send({ status: "OK" });
  });

  return app;
};

module.exports = build;
