const build = require("../../src/app");

let app;
describe("Root Routes", () => {
  beforeEach(() => {
    app = build();
  });
  afterEach(() => {
    app.close();
  });
  it("should return 200", async () => {
    const res = await app.inject({
      url: "/",
    });
    expect(res.statusCode).toBe(200);
    expect(res.json()).toEqual({ status: "OK" });
  });
});
